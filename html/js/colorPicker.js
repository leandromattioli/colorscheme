/* -*- Mode: javascript; js-basic-offset: 4; indent-tabs-mode: nil -*-
 *
 * colorscheme
 * Copyright (C) Leandro Resende Mattioli 2012 <leandro.mattioli@gmail.com>
 *
 */

//Depends:
//color.js :: hsvToRgb, rgbToHex

// Adapted From:
// http://www.ficml.org/jemimap/style/color/wheel.html

//TODO Handle text changed events
//TODO Notify on_color_changed to observers (use input hidden ??)
//TODO Convert this to an object

/** @class ColorPicker **/
function ColorPicker () {
    this.currColor = [60, 80, 100];
    this.previewColor = [60, 80, 100];
}


//==============================================================================
//Public API
//==============================================================================

/**
 * Init function
 **/
ColorPicker.prototype.init = function () {
    this.picker = document.getElementById("colorPicker"); //TODO assert this.picker != null
    this.previewElement = document.getElementById("colorPreview");
    this.picker.onmousemove = this.mouseMoved.bind(this);
    this.picker.onclick = this.mouseClicked.bind(this);
    this.entryR = document.getElementById("colorPickerR");
    this.entryG = document.getElementById("colorPickerG");
    this.entryB = document.getElementById("colorPickerB");
    this.entryH = document.getElementById("colorPickerH");
    this.entryS = document.getElementById("colorPickerS");
    this.entryV = document.getElementById("colorPickerV");
    this.entryHex = document.getElementById("colorPickerHex");
    this.entryR.onchange = this.textChanged.call(this);
    this.entryG.onchange = this.textChanged.call(this);
    this.updateSquareHue();
}


/**
 * Gets the current color as an HSV array.
 *
 * @return  Array       The HSV representation
 **/
ColorPicker.prototype.getCurrentColorAsHSV = function () {
    return this.currColor;
}


/**
 * Gets the current color as an RGB array.
 *
 * @return  Array       The RGB representation
 **/
ColorPicker.prototype.getCurrentColorAsRGB = function () {
    return hsvToRgb(this.currColor[0], this.currColor[1], this.currColor[2]);
}

//==============================================================================
//Event Dispatchers
//==============================================================================

/**
 * Handles a mouse movement in the color picker.
 *
 * @param  Object e     The mouse move event
 **/
ColorPicker.prototype.mouseMoved = function (e) {
	var x = (e.pageX - this.picker.offsetLeft);
	var y = (e.pageY - this.picker.offsetTop);
    if (y > 256)        //out of bounds
    	return false;
    else if (x >= 296)       //value changed in the hue square
    	return this.squareMouseMoved (x, y);
    return false;
}

/**
 * Handles a mouse click in the color picker.
 *
 * @param  Object e     The mouse move event
 **/
ColorPicker.prototype.mouseClicked = function (e) {
	var x = (e.pageX - this.picker.offsetLeft);
	var y = (e.pageY - this.picker.offsetTop);
    if (y > 256)        //out of bounds
    	return false;
    else if (x >= 296)       //value changed in the hue square
        return this.squareMouseClicked (x, y);
    else
    	return this.wheelMouseClicked (x, y);
}

//==============================================================================
//Private utility functions
//==============================================================================
/**
 * Returns the saturation and value represented in the square for position x,y 
 *
 * @param  Number x     The x coordinate
 * @param  Number y     The y coordinate
 * @return Array        Saturation and value components 
 **/
ColorPicker.prototype.squareColorAt = function (x, y) {
    var xside = (x <= 553) ? x - 296 : 256;
    var yside = (y <= 256) ? y : 256;
    return [Math.round(xside / 256 * 100), 
            Math.round((1 - (yside / 256)) * 100)]
}

//==============================================================================
//Event handlers
//==============================================================================

/**
 * Handles a movement in the saturation-value square. Updates previewColor.
 *
 * @param  Number x     The x coordinate
 * @param  Number y     The y coordinate 
 **/
ColorPicker.prototype.squareMouseMoved = function (x, y) {
    satVal = this.squareColorAt(x, y);
    this.previewColor[1] = satVal[0];
    this.previewColor[2] = satVal[1];
    this.updateColorPreview();
    return false;
}

/**
 * Handles a mouse click in the saturation-value square. Updates currColor;
 *
 * @param  Number x     The x coordinate
 * @param  Number y     The y coordinate 
 **/
ColorPicker.prototype.squareMouseClicked = function (x, y) {
    satVal = this.squareColorAt(x, y);
    this.previewColor[1] = satVal[0];
    this.previewColor[2] = satVal[1];
    this.currColor[1] = satVal[0];
    this.currColor[2] = satVal[1];
    if(this.on_color_selected != undefined)    //Notify clients
        this.on_color_selected(this, this.currColor);
    return false;
}

/**
 * Handles a movement in the color wheel.
 *
 * @param  Number x     The x coordinate
 * @param  Number y     The y coordinate 
 **/
ColorPicker.prototype.wheelMouseClicked = function (x, y) {
    //DEBUG
    //console.log("Wheel " + x + ',' + y);
    var h, s, v;
    var cartx = x - 128;
    var carty = 128 - y;
    var cartx2 = cartx * cartx;
    var carty2 = carty * carty;
    var cartxs = (cartx < 0) ? -1 : 1;
    var cartys = (carty < 0) ? -1 : 1;
    var cartxn = cartx / 128;	// normalize x
    var rraw = Math.sqrt(cartx2 + carty2);	// raw radius
    var rnorm = rraw / 128;		// normalized radius
    if (rraw == 0)
        this.currColor = [0, 0, 0];
    else {
	    var arad = Math.acos(cartx / rraw);	// angle in radians 
    	var aradc = (carty >= 0) ? arad : 2 * Math.PI - arad; // correct below axis
	    h = Math.round(360 * aradc / (2 * Math.PI));	// convert to degrees
	    if (rnorm > 1)	// outside circle
	        this.currColor = [0, 0, 100];
    	else if (rnorm >= .5) { // adeg, saturation, value
    	    s = Math.round((1 - ((rnorm - .5) * 2)) * 100);
    	    v = 100;
    	    this.currColor = [h, s, v];
	    }
	    else { //adeg, saturation, value
    	    s = 100;
    	    v = Math.round(rnorm * 2 * 100);
    	    this.currColor = [h, s, v];
	    }
    }
    this.previewColor = this.currColor.slice();
    this.updateSquareHue();
    this.updateColorPreview();
    if(this.on_color_selected != undefined)   //Notify clients
        this.on_color_selected(this, this.currColor);
    return false;
}


ColorPicker.prototype.textChanged = function () {
    //TODO
}

//==============================================================================
//Update
//==============================================================================

/**
 * Updates the saturation-value square.
 **/
ColorPicker.prototype.updateSquareHue = function () {
    var rgb = hsvToRgb(this.currColor[0], 100, 100);
    var hex = rgbToHex(rgb[0], rgb[1], rgb[2]);
	this.picker.style.backgroundColor = hex;
}

/**
 * Updates the color preview box
 **/
ColorPicker.prototype.updateColorPreview = function () {
    //Updating bgcolor
    var rgb1 = hsvToRgb(this.previewColor[0], 
                        this.previewColor[1],
                        this.previewColor[2]);
    var hex1 = rgbToHex(rgb1[0], rgb1[1], rgb1[2]);
    this.previewElement.style.backgroundColor = hex1;
    
    //Updating text
    this.entryR.value = rgb1[0];
    this.entryG.value = rgb1[1];
    this.entryB.value = rgb1[2];
    this.entryH.value = this.previewColor[0];
    this.entryS.value = this.previewColor[1];
    this.entryV.value = this.previewColor[2];
    this.entryHex.value = hex1;        
}

