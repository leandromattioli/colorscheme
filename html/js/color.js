/* -*- Mode: javascript; js-basic-offset: 4; indent-tabs-mode: nil -*-
 *
 * colorscheme
 * Copyright (C) Leandro Resende Mattioli 2012 <leandro.mattioli@gmail.com>
 *
 */
 
//From:
//http://mjijackson.com/2008/02/rgb-to-hsl-and-rgb-to-hsv-color-model-conversion-algorithms-in-javascript
 
/**
 * Converts an RGB color value to HSL. Conversion formula
 * adapted from http://en.wikipedia.org/wiki/HSL_color_space.
 * Assumes r, g, and b are contained in the set [0, 255].
 * Returns h, s, and l in the sets [0, 360], [0, 100] and [0, 100], 
 * respectively.
 *
 * @param   Number  r       The red color value in the set [0, 255]
 * @param   Number  g       The green color value in the set [0, 255]
 * @param   Number  b       The blue color value in the set [0, 255]
 * @return  Array           The HSL representation
 */
function rgbToHsl(r, g, b) {
    r /= 255, g /= 255, b /= 255;
    var max = Math.max(r, g, b), min = Math.min(r, g, b);
    var h, s, l = (max + min) / 2;

    if(max == min){
        h = s = 0; // achromatic
    }else{
        var d = max - min;
        s = l > 0.5 ? d / (2 - max - min) : d / (max + min);
        switch(max){
            case r: h = (g - b) / d + (g < b ? 6 : 0); break;
            case g: h = (b - r) / d + 2; break;
            case b: h = (r - g) / d + 4; break;
        }
        h /= 6;
    }

    return [Math.round(h * 360), Math.round(s * 100), Math.round(l * 100)];
}


/**
 * Converts an HSL color value to RGB. Conversion formula
 * adapted from http://en.wikipedia.org/wiki/HSL_color_space.
 * Assumes: 
 *  - h is contained in the set [0, 360]
 *  - s and l are contained in the set [0, 100]
 * Returns r, g, and b in the set [0, 255].
 *
 * @param   Number  h       The hue in the set [0, 360]
 * @param   Number  s       The saturation in the set [0, 100]
 * @param   Number  l       The lightness in the set [0, 100]
 * @return  Array           The RGB representation
 */
function hslToRgb(h, s, l) {
    h /= 360, s /= 100, l /= 100;
    var r, g, b;

    if(s == 0){
        r = g = b = l; // achromatic
    }else{
        function hue2rgb(p, q, t){
            if(t < 0) t += 1;
            if(t > 1) t -= 1;
            if(t < 1/6) return p + (q - p) * 6 * t;
            if(t < 1/2) return q;
            if(t < 2/3) return p + (q - p) * (2/3 - t) * 6;
            return p;
        }

        var q = l < 0.5 ? l * (1 + s) : l + s - l * s;
        var p = 2 * l - q;
        r = hue2rgb(p, q, h + 1/3);
        g = hue2rgb(p, q, h);
        b = hue2rgb(p, q, h - 1/3);
    }

    return [Math.round(r * 255), Math.round(g * 255), Math.round(b * 255)];
}


/**
 * Converts an RGB color value to HSV. Conversion formula
 * adapted from http://en.wikipedia.org/wiki/HSV_color_space.
 * Assumes r, g, and b are contained in the set [0, 255] and
 * Returns h, s, and v in the sets [0, 360], [0, 100] and [0, 100], 
 * respectively.
 *
 * @param   Number  r       The red color value in the set [0, 255]
 * @param   Number  g       The green color value in the set [0, 255]
 * @param   Number  b       The blue color value in the set [0, 255]
 * @return  Array           The HSV representation
 */
function rgbToHsv(r, g, b) {
    r = r/255, g = g/255, b = b/255;
    var max = Math.max(r, g, b), min = Math.min(r, g, b);
    var h, s, v = max;

    var d = max - min;
    s = max == 0 ? 0 : d / max;

    if(max == min){
        h = 0; // achromatic
    }else{
        switch(max){
            case r: h = (g - b) / d + (g < b ? 6 : 0); break;
            case g: h = (b - r) / d + 2; break;
            case b: h = (r - g) / d + 4; break;
        }
        h /= 6;
    }

    return [Math.round(h * 360), Math.round(s * 100), Math.round( v * 100)];
}


/**
 * Converts an HSV color value to RGB. Conversion formula
 * adapted from http://en.wikipedia.org/wiki/HSV_color_space.
 * Assumes: 
 *  - h is contained in the set [0, 360]
 *  - s and v are contained in the set [0, 100]
 * Returns r, g, and b in the set [0, 255].
 *
 * @param   Number  h       The hue in the set [0, 360]
 * @param   Number  s       The saturation in the set [0, 100]
 * @param   Number  v       The value in the set [0, 100]
 * @return  Array           The RGB representation
 */
function hsvToRgb(h, s, v) {
    h /= 360, s /= 100, v /= 100;
    var r, g, b;

    var i = Math.floor(h * 6);
    var f = h * 6 - i;
    var p = v * (1 - s);
    var q = v * (1 - f * s);
    var t = v * (1 - (1 - f) * s);

    switch(i % 6){
        case 0: r = v, g = t, b = p; break;
        case 1: r = q, g = v, b = p; break;
        case 2: r = p, g = v, b = t; break;
        case 3: r = p, g = q, b = v; break;
        case 4: r = t, g = p, b = v; break;
        case 5: r = v, g = p, b = q; break;
    }

    return [Math.round(r * 255), Math.round(g * 255), Math.round(b * 255)];
}

//From:
//http://stackoverflow.com/questions/5623838/rgb-to-hex-and-hex-to-rgb

/**
 * Gets the Hex triplet string of a RGB color value.
 * Assumes r, g, and b are contained in the set [0, 255].
 *
 * @param   Number  r       The red color value in the set [0, 255]
 * @param   Number  g       The green color value  in the set [0, 255]
 * @param   Number  b       The blue color value  in the set [0, 255]
 * @return  String          Hex triplet
 */
function rgbToHex(r, g, b) {
    return "#" + ((1 << 24) + (r << 16) + (g << 8) + b).toString(16).slice(1);
}


/**
 * Parses an Hex triplet and returns an array with the RGB color value.
 * Returns r, g and b in the set [0, 255].
 *
 * @param   String          Hex triplet
 * @return  Array           The RGB representation
 */
function hexToRgb(hex) {
    var result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex);
    return result ? [parseInt(result[1], 16), 
                     parseInt(result[2], 16),
                     parseInt(result[3], 16)] : null;
}
