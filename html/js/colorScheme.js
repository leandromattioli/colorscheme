/* -*- Mode: javascript; js-basic-offset: 4; indent-tabs-mode: nil -*-
 *
 * colorscheme
 * Copyright (C) Leandro Resende Mattioli 2012 <leandro.mattioli@gmail.com>
 *
 */

//From:
//http://color-wheel-pro.com/color-schemes.html

//TODO YUIDoc this module

/** @class ColorScheme **/
function ColorScheme () {
    this.mainColor = [60, 80, 100];
    this.currScheme = 'mono';
}

//==============================================================================
//Public API
//==============================================================================

ColorScheme.prototype.init = function() {
    this.htmlTemplate = '<div class="colorBox"> \
                         <div style="background-color: {HEX}">&nbsp;</div> \
                         {TXT}</div>';
    this.form = document.getElementById("colorSchemeChoice");
    this.view = document.getElementById("colorSchemeView");
    var inputs = this.form.getElementsByTagName("input");
    for(var x = 0 ; x < inputs.length ; x++) {
        inputs[x].onchange = this.schemeChanged.bind(this);
    }
    this.schemeChanged(null);
}

//==============================================================================
//Event handlers
//==============================================================================

ColorScheme.prototype.colorChanged = function (source, color) {
    this.mainColor = color;
    this.render ();
}

ColorScheme.prototype.schemeChanged = function(e) {
    //Iterating radio buttons
    var radios = this.form.elements["colorScheme"];
    for(var x = 0 ; x < radios.length ; x++)
        if(radios[x].checked)
            this.currScheme = radios[x].value;
    //Updating view
    if(this.mainColor != null)
        this.render ();
}

//==============================================================================
//Update functions
//==============================================================================
ColorScheme.prototype.render = function () {
    var templateCopy;
    var hex;
    var hsv;
    var rgb;
    var text;
    var others;
    
    //Adding main color to the view
    hsv = this.mainColor;
    rgb = hsvToRgb(hsv[0], hsv[1], hsv[2]);
    hex = rgbToHex(rgb[0], rgb[1], rgb[2]);
    text = 'Hex : ' + hex + '<br />' +
           'RGB : ' + rgb[0] + ',' + rgb[1] + ',' + rgb[2] + '<br />' +
           'HSV : ' + hsv[0] + ',' + hsv[1] + ',' + hsv[2] + '<br />';
    templateCopy = this.htmlTemplate.replace("{HEX}", hex);
    templateCopy = templateCopy.replace("{TXT}", text)
    this.view.innerHTML = templateCopy;

    //Getting other colors
    switch(this.currScheme) {
        case 'mono':
            others = this.getMonochromatic(hsv[0], hsv[1], hsv[2]);
            break;
        case 'analogous':
            others = this.getAnalogous(hsv[0], hsv[1], hsv[2]);
            break;
        case 'complement':
            others = [this.getComplement(hsv[0], hsv[1], hsv[2])];
            break; 
        case 'splitComplement':
            others = this.getSplitComplements(hsv[0], hsv[1], hsv[2]);
            break;
        case 'triads':
            others = this.getTriads(hsv[0], hsv[1], hsv[2]);
            break;
        case 'tetrads':
            others = this.getTetrads(hsv[0], hsv[1], hsv[2]);
            break;
    }
    
    //Adding other colors to the view
    for(var x = 0 ; x < others.length ; x++) {
        //Replace this routine by a method addColor ??
        hsv = others[x];
        rgb = hsvToRgb(hsv[0], hsv[1], hsv[2]);
        hex = rgbToHex(rgb[0], rgb[1], rgb[2]);
        text = 'Hex : ' + hex + '<br />' +
               'RGB : ' + rgb[0] + ',' + rgb[1] + ',' + rgb[2] + '<br />' +
               'HSV : ' + hsv[0] + ',' + hsv[1] + ',' + hsv[2] + '<br />';
        templateCopy = this.htmlTemplate.replace("{HEX}", hex);
        templateCopy = templateCopy.replace("{TXT}", text)
        this.view.innerHTML += templateCopy;
    }
}

//==============================================================================
//Core
//==============================================================================

/**
 * Get the complement of a HSV color.
 *
 * @param   Number  h       The hue in the set [0, 360]
 * @param   Number  s       The saturation in the set [0, 100]
 * @param   Number  v       The value in the set [0, 100]
 * @return  Array           The complement color, represented in HSV space
 */
ColorScheme.prototype.getComplement = function (h, s, v) {
    var newHue = (h + 180) % 360; //circular increment
    return [Math.round(newHue), Math.round(s), Math.round(v)];
}

/**
 * Get the split complements of a HSV color.
 *
 * @param   Number  h       The hue in the set [0, 360]
 * @param   Number  s       The saturation in the set [0, 100]
 * @param   Number  v       The value in the set [0, 100]
 * @return  Array[Array]    The split complements, represented in HSV
 */
ColorScheme.prototype.getSplitComplements = function (h, s, v) {
    var offsetAngle = 24; //360 / 15
    var hue1 = (h + 180 - offsetAngle) % 360;
    var hue2 = (h + 180 + offsetAngle) % 360;
    return [[Math.round(hue1), Math.round(s), Math.round(v)],
            [Math.round(hue2), Math.round(s), Math.round(v)]];  
}


/**
 * Get the other three members of the tetrads formation of a HSV color.
 *
 * @param   Number  h       The hue in the set [0, 360]
 * @param   Number  s       The saturation in the set [0, 100]
 * @param   Number  v       The value in the set [0, 100]
 * @return  Array[Array]    The other colors for the tetrads formations, in HSV
 */
ColorScheme.prototype.getTetrads = function (h, s, v) {
    var hue1 = (h + 90) % 360;
    var hue2 = (h + 180) % 360;
    var hue3 = (h + 270) % 360;
    return [[Math.round(hue1), Math.round(s), Math.round(v)],
            [Math.round(hue2), Math.round(s), Math.round(v)],
            [Math.round(hue3), Math.round(s), Math.round(v)]]; 
}


/**
 * Get the other two members of the triads formation of a HSV color.
 *
 * @param   Number  h       The hue in the set [0, 360]
 * @param   Number  s       The saturation in the set [0, 100]
 * @param   Number  v       The value in the set [0, 100]
 * @return  Array[Array]    The other colors for the triads formations, in HSV
 */
ColorScheme.prototype.getTriads = function (h, s, v) {
    var hue1 = (h + 120) % 360;
    var hue2 = (h - 120) % 360;
    if (hue2 < 0)
        hue2 += 360;
    return [[Math.round(hue1), Math.round(s), Math.round(v)],
            [Math.round(hue2), Math.round(s), Math.round(v)]]; 
}


/**
 * Get the other two members of the analogous scheme of a HSV color.
 *
 * @param   Number  h       The hue in the set [0, 360]
 * @param   Number  s       The saturation in the set [0, 100]
 * @param   Number  v       The value in the set [0, 100]
 * @return  Array[Array]    The other colors of the analogous scheme, in HSV
 */
ColorScheme.prototype.getAnalogous = function (h, s, v) {
    var hue1 = (h + 30) % 360;
    var hue2 = (h - 30) % 360;
    if (hue2 < 0)
        hue2 += 360;    
    return [[Math.round(hue1), Math.round(s), Math.round(v)],
            [Math.round(hue2), Math.round(s), Math.round(v)]]; 
}


/**
 * Get the other two members of the monochromatic scheme of a HSV color.
 *
 * @param   Number  h       The hue in the set [0, 360]
 * @param   Number  s       The saturation in the set [0, 100]
 * @param   Number  v       The value in the set [0, 100]
 * @return  Array[Array]    The other colors of the monochromatic scheme, in HSV
 */
ColorScheme.prototype.getMonochromatic = function (h, s, v) {
    if (s < 10) {
        var sat1 = (s + 100 / 3) % 100;
        var sat2 = (s + 200 / 3) % 100;
        return [[Math.round(h), Math.round(sat1), Math.round(v)], 
                [Math.round(h), Math.round(sat2), Math.round(v)]];
    }
    else {
        var val1 = (v + 100 / 3) % 100;
        var val2 = (v + 200 / 3) % 100;
        return [[Math.round(h), Math.round(s), Math.round(val1)], 
                [Math.round(h), Math.round(s), Math.round(val2)]];
    }
}
